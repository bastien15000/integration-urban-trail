import './style.scss';
import '@fortawesome/fontawesome-free/js/fontawesome'
import '@fortawesome/fontawesome-free/js/solid'
import '@fortawesome/fontawesome-free/js/regular'
import '@fortawesome/fontawesome-free/js/brands'
import './scripts/nav';

const menu = document.getElementById("menu_burger");
const content = document.getElementById("menu_burger_content");
menu.addEventListener('click', ()=>{
    if(!content.classList.contains("open") && !content.classList.contains("close")){
        content.classList.add("open");
    }
    if(content.classList.contains("open")){
        content.classList.remove("open");
        content.classList.add("close");
    }
    if(content.classList.contains("close")){
        content.classList.remove("close");
        content.classList.add("open");
    }
})

const close = document.getElementById("menu_burger_content_close");
close.addEventListener('click', ()=>{
    content.classList.remove("open");
    content.classList.add("close");
})

