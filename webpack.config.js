const path = require('path');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const CopyPlugin = require("copy-webpack-plugin");

module.exports = {
  entry: './src/index.js',
  output: {
    filename: 'main.js',
    path: path.resolve(__dirname, 'dist'),
  },
  plugins: [new MiniCssExtractPlugin(), 
    new CopyPlugin({
      patterns: [
        { from: "index.html", to: "./index.html" },
        { from: "assets", to: "./assets" },
      ],
    }),
  ],
  module: {
      rules: [
          {
              test: /\.s[ac]ss$/i,
              use: [MiniCssExtractPlugin.loader, 'css-loader','sass-loader'],
          },
        ],
    },
    watch: true,
};